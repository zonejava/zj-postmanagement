# Zone Java project - Post-management API
## Description
This API handle posts management of Zone Java project.
Follow [this link](https://gitlab.com/zonejava/conception/-/blob/master/README.md) to see the project overview.
## Development
It has been developed with IntelliJ IDEA 2020.3 (Ultimate Edition) and Java JDK 11.0.8.

It uses SpringBoot 2.2.5.RELEASE.You will find dependencies uses details in pom.xml files.
## Installation & Run
1) This API needs a Postgres Database.
   You need to create it, add your configuration to an application.yml in a repository and launch a Config Server [like this one](https://gitlab.com/zonejava/zj-configserver)
   (you need to inquire the address of the config server in the bootstrap.yml of this API or avoid using
   it and put DB configuration in the bootstrap.yml).
   Then you can launch the API by two ways :
* From your favorite IDE like you are used to.
* From your terminal, by going to root directory and launch `mvn springboot:run` command.
  By default, it exposes on port 8083 (you can change it in bootstrap.yml or inquire an environement
  variable like `SERVER_PORT='yourCustomPort'`)

2) This API is configured to join a [Discovery server](https://gitlab.com/zonejava/zj-discoveryserver) and
   register itself. If you want to test it as a standalone API, you can remove 'Eureka configuration' in
   bootstrap.yml file.

(you need Java 11+ on your machine but maven is optional because of maven wrapper included in the repository)
## Endpoints
* coming soon a detail list of endpoint
## Owners
Arnaud Laval - arnaudlaval33@gmail.com

Mickaël Coz - coz.mickael@gmail.com

The Zone Java project is developed as part of final project of DA Java cursus (OpenClassrooms)
