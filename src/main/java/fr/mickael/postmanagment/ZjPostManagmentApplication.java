package fr.mickael.postmanagment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZjPostManagmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZjPostManagmentApplication.class, args);
    }

}
